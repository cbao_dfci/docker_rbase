### R 3.4.0 (rbase)

This repository contains the R-3.4.

## Table of Contents
[R](https://cran.rstudio.com/bin/linux/ubuntu/xenial/)

## Authors
* **Chunyang Bao** - *Initial work* - [cbao_dfci](https://bitbucket.org/cbao_dfci/)

## License
Licensed under the BSD License. See the [LICENSE.txt](https://github.com/broadinstitute/gatk/blob/master/LICENSE.TXT) file.
