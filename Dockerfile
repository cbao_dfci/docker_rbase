# Using OpenJDK 8
# This Dockerfile does not require any files that are in the GATK4 repo.
FROM ubuntu:16.04

#### Basic image utilities
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y python && \
    apt-get install -y wget \
    curl libcurl4-gnutls-dev \
    libssl-dev \
    unzip \
    less \
    bedtools \
    samtools \
    openjdk-8-jdk \
    software-properties-common && \
    apt-get -y clean  && \
    apt-get -y autoclean  && \
    apt-get -y autoremove

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]

ENV JAVA_LIBRARY_PATH /usr/lib/jni
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/

RUN java -version

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9 && \
    add-apt-repository "deb http://cran.rstudio.com/bin/linux/ubuntu xenial/" && \
    apt-get update && \
    apt-get install -y --force-yes \
        r-base-dev=3.4.0-1xenial0 \
        r-base-core=3.4.0-1xenial0 && \
    apt-get -y clean && \
    apt-get -y autoremove && \
    apt-get  -y autoclean

# R environment
RUN Rscript -e "install.packages('https://bioconductor.org/packages/3.6/bioc/src/contrib/BiocInstaller_1.28.0.tar.gz', repos=NULL, type='source', clean=TRUE)" && \
    Rscript -e "install.packages('https://cran.r-project.org/src/contrib/getopt_1.20.2.tar.gz', repos=NULL, type='source', clean=TRUE)" && \
    Rscript -e "install.packages('https://cran.r-project.org/src/contrib/optparse_1.6.0.tar.gz', repos=NULL, type='source', clean=TRUE)" && \
    Rscript -e "install.packages('https://cran.r-project.org/src/contrib/data.table_1.11.4.tar.gz', repos=NULL, type='source', clean=TRUE)" && \
    Rscript -e "install.packages('devtools', repos='http://cran.us.r-project.org', clean=TRUE)"

# Deleting unneeded caches
RUN rm -rf /var/lib/apt/lists/*
